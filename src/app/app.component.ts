import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import {NavbarComponent} from './navbar/navbar.component';
import {TypeaheadComponent} from './navbar/typeahead/typeahead.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, TypeaheadComponent, NavbarComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent {
  title = 'angular-ng-bootstrap';
}
