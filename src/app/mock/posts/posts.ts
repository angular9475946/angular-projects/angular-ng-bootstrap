import { Post } from './post';

export const POSTS: Post[] = [
  {
    id: 'p1',
    title: 'The Rise of Angular in Web Development',
    content: 'Angular has become one of the most popular frameworks for building dynamic web applications. This post explores the key features and benefits of using Angular for modern web development...',
    authorId: 'a1',
    publishedDate: '2024-01-15'
  },
  {
    id: 'p2',
    title: 'Understanding Reactive Programming in Angular',
    content: 'Reactive programming is a powerful paradigm for handling asynchronous data streams in Angular. In this post, we dive deep into the concepts of reactive programming and how to implement it using RxJS...',
    authorId: 'a2',
    publishedDate: '2024-02-10'
  },
  {
    id: 'p3',
    title: 'Building Progressive Web Apps with Angular',
    content: 'Progressive Web Apps (PWAs) offer a native app-like experience on the web. This post provides a comprehensive guide on how to build PWAs using Angular, from setup to deployment...',
    authorId: 'a3',
    publishedDate: '2024-03-05'
  }
];
