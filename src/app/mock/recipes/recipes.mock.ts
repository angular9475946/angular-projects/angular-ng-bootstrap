import { Recipe } from './recipe';

export const RECIPES: Recipe[] = [
  {
    id: 'r1',
    title: 'Spaghetti Carbonara',
    description: 'A classic Italian pasta dish made with eggs, cheese, pancetta, and pepper.',
    ingredients: [
      '200g spaghetti',
      '100g pancetta',
      '2 large eggs',
      '50g pecorino cheese',
      '50g parmesan',
      'Freshly ground black pepper',
      'Salt',
      '2 cloves garlic',
      '1 tbsp olive oil'
    ],
    instructions: [
      'Bring a large pot of salted water to a boil. Cook the spaghetti according to the package instructions.',
      'Meanwhile, heat the oil in a large skillet over medium heat. Add the pancetta and garlic, cooking until the pancetta is crispy.',
      'In a bowl, whisk together the eggs, pecorino, and parmesan.',
      'Drain the spaghetti and add to the skillet with the pancetta. Remove from heat and quickly add the egg mixture, stirring constantly until the pasta is coated.',
      'Season with pepper and serve immediately.'
    ],
    imageUrl: 'https://example.com/images/spaghetti-carbonara.jpg'
  },
  {
    id: 'r2',
    title: 'Chicken Curry',
    description: 'A flavorful chicken curry made with a blend of spices, tomatoes, and coconut milk.',
    ingredients: [
      '500g chicken breast, diced',
      '1 large onion, chopped',
      '3 cloves garlic, minced',
      '1 tbsp ginger, minced',
      '2 tbsp curry powder',
      '1 tsp turmeric',
      '1 tsp cumin',
      '1 can (400ml) coconut milk',
      '1 can (400g) diced tomatoes',
      'Salt and pepper',
      '2 tbsp vegetable oil',
      'Fresh cilantro, for garnish'
    ],
    instructions: [
      'Heat the oil in a large pot over medium heat. Add the onion, garlic, and ginger, cooking until the onion is translucent.',
      'Add the chicken and cook until browned on all sides.',
      'Stir in the curry powder, turmeric, and cumin, cooking for another minute until fragrant.',
      'Add the diced tomatoes and coconut milk, bringing the mixture to a simmer.',
      'Cook for 20-25 minutes until the chicken is cooked through and the sauce has thickened.',
      'Season with salt and pepper to taste. Garnish with fresh cilantro before serving.'
    ],
    imageUrl: 'https://example.com/images/chicken-curry.jpg'
  },
  {
    id: 'r3',
    title: 'Vegetable Stir-Fry',
    description: 'A quick and healthy vegetable stir-fry with a savory soy sauce.',
    ingredients: [
      '1 broccoli head, cut into florets',
      '1 red bell pepper, sliced',
      '1 yellow bell pepper, sliced',
      '2 carrots, julienned',
      '1 zucchini, sliced',
      '2 tbsp soy sauce',
      '1 tbsp oyster sauce',
      '1 tbsp hoisin sauce',
      '2 cloves garlic, minced',
      '1 tsp ginger, minced',
      '2 tbsp vegetable oil',
      'Cooked rice, for serving'
    ],
    instructions: [
      'Heat the oil in a large skillet or wok over medium-high heat. Add the garlic and ginger, cooking until fragrant.',
      'Add the broccoli, bell peppers, and carrots, cooking for 5-7 minutes until they start to soften.',
      'Stir in the zucchini and cook for another 2-3 minutes.',
      'In a small bowl, mix together the soy sauce, oyster sauce, and hoisin sauce.',
      'Pour the sauce over the vegetables, stirring to coat evenly. Cook for another 2-3 minutes until the vegetables are tender-crisp.',
      'Serve the stir-fry over cooked rice.'
    ],
    imageUrl: 'https://example.com/images/vegetable-stir-fry.jpg'
  }
];
