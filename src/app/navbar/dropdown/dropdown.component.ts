import { Component } from '@angular/core';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import {ListItemComponent} from '../list-item/list-item.component';

@Component({
  selector: 'app-dropdown',
  standalone: true,
  imports: [NgbDropdownModule, ListItemComponent],
  templateUrl: './dropdown.component.html',
  styleUrl: './dropdown.component.scss'
})
export class DropdownComponent {

}
