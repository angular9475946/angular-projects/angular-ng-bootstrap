import { Component } from '@angular/core';
import {ProfileComponent} from '../shared/profile/profile.component';
import {DropdownComponent} from './dropdown/dropdown.component';
import {TypeaheadComponent} from './typeahead/typeahead.component';

@Component({
  selector: 'app-navbar',
  standalone: true,
  imports: [
    TypeaheadComponent,
    DropdownComponent,
    ProfileComponent
  ],
  templateUrl: './navbar.component.html',
  styleUrl: './navbar.component.scss'
})
export class NavbarComponent {

}
