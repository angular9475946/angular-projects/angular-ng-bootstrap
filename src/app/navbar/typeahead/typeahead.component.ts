import {NgIf} from '@angular/common';
import { Component } from '@angular/core';
import { NgbTypeaheadModule } from '@ng-bootstrap/ng-bootstrap';
import {
  Observable,
  OperatorFunction,
  tap
} from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import { FormsModule } from '@angular/forms';

const statesHistory: { name: string;}[] = [
  { name: 'Alabama'},
  { name: 'Alaska'},
  { name: 'Arizona'},
  { name: 'Arkansas'},
  { name: 'California'}
];

const statesWithFlags: { name: string; flag: string }[] = [
  { name: 'Alabama', flag: '5/5c/Flag_of_Alabama.svg/45px-Flag_of_Alabama.svg.png' },
  { name: 'Alaska', flag: 'e/e6/Flag_of_Alaska.svg/43px-Flag_of_Alaska.svg.png' },
  { name: 'Arizona', flag: '9/9d/Flag_of_Arizona.svg/45px-Flag_of_Arizona.svg.png' },
  { name: 'Arkansas', flag: '9/9d/Flag_of_Arkansas.svg/45px-Flag_of_Arkansas.svg.png' },
  { name: 'California', flag: '0/01/Flag_of_California.svg/45px-Flag_of_California.svg.png' },
  { name: 'Colorado', flag: '4/46/Flag_of_Colorado.svg/45px-Flag_of_Colorado.svg.png' },
  { name: 'Connecticut', flag: '9/96/Flag_of_Connecticut.svg/39px-Flag_of_Connecticut.svg.png' },
  { name: 'Delaware', flag: 'c/c6/Flag_of_Delaware.svg/45px-Flag_of_Delaware.svg.png' },
  { name: 'Florida', flag: 'f/f7/Flag_of_Florida.svg/45px-Flag_of_Florida.svg.png' }
];

@Component({
  selector: 'app-typeahead',
  standalone: true,
  imports: [NgbTypeaheadModule, FormsModule, NgIf],
  templateUrl: './typeahead.component.html',
  styleUrl: './typeahead.component.scss'
})
export class TypeaheadComponent {

  search: OperatorFunction<string, readonly { name: string; flag: string }[]> = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      tap((term) => this.hasText = term.length > 0),
      map((term) =>
        term === ''
          ? []
          : statesWithFlags.filter((v) => v.name.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10),
      ),
    );

  formatter = (x: { name: string }) => x.name;

  isFocusing = false;
  hasText = false;

  inputFocus() {
    this.isFocusing = !this.hasText ?? true;
  }

  inputType() {
    this.isFocusing = this.hasText ?? false;
  }

  inputFocusOut() {
    this.isFocusing = false;
  }
}
